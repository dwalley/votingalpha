pragma solidity ^0.4.18;

contract Greetings {
  string message;

  function Greetings() public {
    message = "Ready.";
  }

  function SetMessage(string value) public {
    message = value;
  }

  // View indicates that this call won't cause a change - aka, it's "read only" and creates no transaction
  function Message() public view returns (string) { return message; }
}
