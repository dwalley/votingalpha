pragma solidity ^0.4.18;


contract VotingBallot {
    //================================================================================
    address mOwner;

    string mName;

    VotingOption[] public mOptions;
    mapping(address => Voter) public mVoters;


    //================================================================================
    //--------------------------------------------------------------------------------
    function VotingBallot(string name) public {
        mOwner = msg.sender;
        mName = name;
    }

    //--------------------------------------------------------------------------------
    function Name() public view returns (string) public { return mName; }

    //--------------------------------------------------------------------------------
    function SetName(string name) public OwnerOnly {
      mName = name;
    }



    //function AddOption(int test) OwnerOnly {
    //}



    //================================================================================
    //********************************************************************************
    // Modifer to allow only the owning account to call a function
    modifier OwnerOnly {
        //if (msg.sender != mOwner)
        //    throw;
        require(msg.sender == mOwner);
        _; // Continue execution of method
    }

    //********************************************************************************
    struct VotingOption {
        string name;
        uint votes; // Number of votes given to this option
    }

    //********************************************************************************
    struct Voter {
        bool eligible; // Controls whether voter is allowed to cast votes
        bool voted;
        uint votedFor;
    }
}
