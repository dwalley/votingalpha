## Notes
For the moment this is just a working example of using Nethereum to:

- Connect to a ganache node.
- Deploy a contract.
- Call read only functions on the contract.
- Call write functions on the contract, via transactions.

Will be using it as a basis to both figure out Nethereum, and build an initial rough prototype of a voting system.
Initial version will be a bit messy, but goal is to just make it work.
Next step will be to refactor the blockchain related code out into a set of classes which could be re-used in any .Net context (e.g. ASP.net backend).

## Usage
1. Install Ganache (http://truffleframework.com/ganache/).
2. For the moment, just have the node exposed on 127.0.0.1:7545. Everything else can be left at default.
3. Launch Ganache, and copy one of the account addresses in the Accounts section. Paste this into Program.cs line 17:
   private const string BALANCE_ADDRESS = address;
   This is the account which requests and pays for deployment of the contract.
4. In theory there's no other dependencies, but I haven't checked.
5. Build and run - the program will spit out some account information from the Ganache node, query some balances, deploy the contract, and make some calls to said contract.

## Workflow
- As far as I've currently determined, contracts have to be compiled manually when working with Nethereum (the .Net solidity plugins are out of date).
- Contracts/Greetings.sol was compiled via http://remix.ethereum.org/, by pasting the contract code and pressing the Details button in the compile tab
  of the right pane. The bytecode and abi sections provide the output used for GREETINGS_BYTECODE and GREETINGS_ABI in Program.cs, which is then fed
  to the deployment transaction.
- Node/NPM have library support for using the solidity compiler directly - will be worth seeing if Nethereum or something else provides a .Net alternative.










## Template

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).