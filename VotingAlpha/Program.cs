﻿using Nethereum.Contracts;
using Nethereum.Hex.HexConvertors.Extensions;
using Nethereum.Hex.HexTypes;
using Nethereum.Web3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;



namespace VotingAlpha {
    class Program {
        //================================================================================
        //private const string NODE_URL = "http://192.168.241.129:7545";
        private const string NODE_URL = "http://localhost:7545";
        private const string BALANCE_ADDRESS = "0x6241dC9EA4282bB46E7422d29206BD38957661AC";
        // Compiled via https://remix.ethereum.org/
        private const string GREETINGS_BYTECODE = "0x6060604052341561000f57600080fd5b6040805190810160405280600681526020017f52656164792e00000000000000000000000000000000000000000000000000008152506000908051906020019061005a929190610060565b50610105565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106100a157805160ff19168380011785556100cf565b828001600101855582156100cf579182015b828111156100ce5782518255916020019190600101906100b3565b5b5090506100dc91906100e0565b5090565b61010291905b808211156100fe5760008160009055506001016100e6565b5090565b90565b6102e3806101146000396000f30060606040526004361061004c576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff16806388fabb3a14610051578063a1fb1de9146100ae575b600080fd5b341561005c57600080fd5b6100ac600480803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509190505061013c565b005b34156100b957600080fd5b6100c1610156565b6040518080602001828103825283818151815260200191508051906020019080838360005b838110156101015780820151818401526020810190506100e6565b50505050905090810190601f16801561012e5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b80600090805190602001906101529291906101fe565b5050565b61015e61027e565b60008054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156101f45780601f106101c9576101008083540402835291602001916101f4565b820191906000526020600020905b8154815290600101906020018083116101d757829003601f168201915b5050505050905090565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061023f57805160ff191683800117855561026d565b8280016001018555821561026d579182015b8281111561026c578251825591602001919060010190610251565b5b50905061027a9190610292565b5090565b602060405190810160405280600081525090565b6102b491905b808211156102b0576000816000905550600101610298565b5090565b905600a165627a7a72305820dd961326a07b2884f213719c6e8ce1b5d8faf9e8c20138bf200b0b6c8e4e6be80029"; 
        private const string GREETINGS_ABI = @"[{""constant"":false,""inputs"":[{""name"":""value"",""type"":""string""}],""name"":""SetMessage"",""outputs"":[],""payable"":false,""stateMutability"":""nonpayable"",""type"":""function""},{""constant"":true,""inputs"":[],""name"":""Message"",""outputs"":[{""name"":"""",""type"":""string""}],""payable"":false,""stateMutability"":""view"",""type"":""function""},{""inputs"":[],""payable"":false,""stateMutability"":""nonpayable"",""type"":""constructor""}]";

        private const string BALLOT_BYTECODE = "0x608060405234801561001057600080fd5b506040516106b53803806106b583398101806040528101908080518201929190505050336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055508060019080519060200190610089929190610090565b5050610135565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106100d157805160ff19168380011785556100ff565b828001600101855582156100ff579182015b828111156100fe5782518255916020019190600101906100e3565b5b50905061010c9190610110565b5090565b61013291905b8082111561012e576000816000905550600101610116565b5090565b90565b610571806101446000396000f300608060405260043610610062576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680634df9dcd314610067578063587b698e146100d05780638052474d1461013d578063ef9e7cb2146101cd575b600080fd5b34801561007357600080fd5b506100ce600480360381019080803590602001908201803590602001908080601f016020809104026020016040519081016040528093929190818152602001838380828437820191505050505050919291929050505061027a565b005b3480156100dc57600080fd5b50610111600480360381019080803573ffffffffffffffffffffffffffffffffffffffff1690602001909291905050506102ef565b604051808415151515815260200183151515158152602001828152602001935050505060405180910390f35b34801561014957600080fd5b50610152610333565b6040518080602001828103825283818151815260200191508051906020019080838360005b83811015610192578082015181840152602081019050610177565b50505050905090810190601f1680156101bf5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b3480156101d957600080fd5b506101f8600480360381019080803590602001909291905050506103d5565b6040518080602001838152602001828103825284818151815260200191508051906020019080838360005b8381101561023e578082015181840152602081019050610223565b50505050905090810190601f16801561026b5780820380516001836020036101000a031916815260200191505b50935050505060405180910390f35b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156102d557600080fd5b80600190805190602001906102eb9291906104a0565b5050565b60036020528060005260406000206000915090508060000160009054906101000a900460ff16908060000160019054906101000a900460ff16908060010154905083565b606060018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156103cb5780601f106103a0576101008083540402835291602001916103cb565b820191906000526020600020905b8154815290600101906020018083116103ae57829003601f168201915b5050505050905090565b6002818154811015156103e457fe5b9060005260206000209060020201600091509050806000018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156104905780601f1061046557610100808354040283529160200191610490565b820191906000526020600020905b81548152906001019060200180831161047357829003601f168201915b5050505050908060010154905082565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106104e157805160ff191683800117855561050f565b8280016001018555821561050f579182015b8281111561050e5782518255916020019190600101906104f3565b5b50905061051c9190610520565b5090565b61054291905b8082111561053e576000816000905550600101610526565b5090565b905600a165627a7a72305820b66ab193308ec514f3aace6d724ed83b82112fe8a77c10cba8d25f5c93d9ed460029";
        private const string BALLOT_ABI = @"[{""constant"":false,""inputs"":[{""name"":""name"",""type"":""string""}],""name"":""SetName"",""outputs"":[],""payable"":false,""stateMutability"":""nonpayable"",""type"":""function""},{""constant"":true,""inputs"":[{""name"":"""",""type"":""address""}],""name"":""mVoters"",""outputs"":[{""name"":""eligible"",""type"":""bool""},{""name"":""voted"",""type"":""bool""},{""name"":""votedFor"",""type"":""uint256""}],""payable"":false,""stateMutability"":""view"",""type"":""function""},{""constant"":true,""inputs"":[],""name"":""Name"",""outputs"":[{""name"":"""",""type"":""string""}],""payable"":false,""stateMutability"":""view"",""type"":""function""},{""constant"":true,""inputs"":[{""name"":"""",""type"":""uint256""}],""name"":""mOptions"",""outputs"":[{""name"":""name"",""type"":""string""},{""name"":""votes"",""type"":""uint256""}],""payable"":false,""stateMutability"":""view"",""type"":""function""},{""inputs"":[{""name"":""name"",""type"":""string""}],""payable"":false,""stateMutability"":""nonpayable"",""type"":""constructor""}]";


        //================================================================================
        //--------------------------------------------------------------------------------
        static void Main(string[] args) {
            try { Run(); }
            catch (Exception e) {
                LogLine(ConsoleColor.Red, "\nERROR ", ConsoleColor.Gray, InnermostException(e).Message);          
            }
            LogLine("Press any key to continue...");
            Console.ReadLine();
        }
        
        //--------------------------------------------------------------------------------
        static void Run() {
            // Connect (ganache)
            LogLine("===============================================================================");
            LogLine("Establishing Web3...");
            Web3 web3 = new Web3(NODE_URL);

            // Create account
            Log("New account: ");
            string address = web3.Personal.NewAccount.SendRequestAsync("password").Result;
            LogLine(ConsoleColor.DarkGreen, address);

            // Create account
            // Having trouble getting this approach to work
            /*Log("New account: [");
            var accountECKey = Nethereum.Signer.EthECKey.GenerateKey();
            string address = accountECKey.GetPublicAddress();
            var accountPrivateKey = accountECKey.GetPrivateKeyAsBytes().ToHex();
            var account = new Nethereum.Web3.Accounts.Account(accountPrivateKey);
            LogLine(ConsoleColor.DarkGreen, address, ConsoleColor.Gray, "]");*/

            // Unlock account (not required for the built in Ganache accounts)
            Log("Unlock account: ");
            var unlockAccountResult = web3.Personal.UnlockAccount.SendRequestAsync(address, "password", 120).Result;
            if (unlockAccountResult)
                LogLine(ConsoleColor.Green, "success");
            else {
                LogLine(ConsoleColor.Red, "failure");
                return;
            }

            // Send ether
            Log("Send ether: ");
            var sendEtherReceipt = web3.TransactionManager.SendTransactionAsync(BALANCE_ADDRESS, address, new HexBigInteger(Web3.Convert.ToWei(1))).Result;
            LogLine(ConsoleColor.Magenta, sendEtherReceipt);

            // Balance check
            Log("[", ConsoleColor.DarkGreen, address, ConsoleColor.Gray, "].balance: ");
            var balance = web3.Eth.GetBalance.SendRequestAsync(address).Result;
            var ether = Web3.Convert.FromWei(balance.Value);
            LogLine(ConsoleColor.DarkCyan, ether, ConsoleColor.DarkCyan, ConsoleColor.Gray, " ether");

            // Accounts
            LogLine("Accounts:");
            //var accounts = web3.Personal.ListAccounts.SendRequestAsync().Result;
            var accounts = web3.Eth.Accounts.SendRequestAsync().Result;
            for (int i = 0; i < accounts.Length; ++i) {
                LogLine(" " + i + ". ", ConsoleColor.DarkGreen, accounts[i]);
            }

            // Deploy contract
            Log("Deploying contract: ");
            var contractTransaction = web3.Eth.DeployContract.SendRequestAsync(GREETINGS_ABI, GREETINGS_BYTECODE, address, new HexBigInteger(4700000)).Result;
            LogLine(ConsoleColor.Magenta, contractTransaction);

            // Start mining
            //var mineResult = await web3.Miner.Start.SendRequestAsync(6);
            //Assert.True(mineResult);

            // Contract address (fun: Ganache 1.02 was incorrectly returning the status field as an int, causing this to break)
            Log("Contract address: ");
            var contractReceipt = web3.Eth.Transactions.GetTransactionReceipt.SendRequestAsync(contractTransaction).Result;
            var contractAddress = contractReceipt.ContractAddress;
            LogLine(ConsoleColor.Magenta, contractAddress);

            // Contract
            var contract = web3.Eth.GetContract(GREETINGS_ABI, contractAddress);
            var contractSetMessageFunction = contract.GetFunction("SetMessage");
            var contractMessageFunction = contract.GetFunction("Message");

            // Get message
            Log("Contract.Message: ");
            var contractMessage = contractMessageFunction.CallAsync<string>().Result;
            LogLine(ConsoleColor.DarkYellow, contractMessage);

            // Set message - send
            Log("Setting message: ");
            var setMessageTransaction = contractSetMessageFunction.SendTransactionAsync(address, new HexBigInteger(100000), null, null, "abc123").Result;
            LogLine(ConsoleColor.Magenta, setMessageTransaction);

            // Set message - wait
            Log("Awaiting message set: ");
            var setMessageReceipt = web3.Eth.Transactions.GetTransactionReceipt.SendRequestAsync(setMessageTransaction).Result;
            LogLine(ConsoleColor.Magenta, setMessageReceipt.TransactionIndex.Value + "/", ConsoleColor.DarkMagenta, setMessageReceipt.TransactionHash);

            // Get message
            Log("Contract.Message: ");
            contractMessage = contractMessageFunction.CallAsync<string>().Result;
            LogLine(ConsoleColor.DarkYellow, contractMessage);

            // Set message (better method)
            Log("Setting message with receipt: ");
            setMessageReceipt = contractSetMessageFunction.SendTransactionAndWaitForReceiptAsync(address, new HexBigInteger(100000), null, null, "def456").Result;
            LogLine(ConsoleColor.Magenta, setMessageReceipt.TransactionIndex.Value + "/", ConsoleColor.DarkMagenta, setMessageReceipt.TransactionHash);

            // Get message
            Log("Contract.Message: ");
            contractMessage = contractMessageFunction.CallAsync<string>().Result;
            LogLine(ConsoleColor.DarkYellow, contractMessage);

            // BALLOT
            LogLine("-------------------------------------------------------------------------------");

            // Deploy ballot
            Log("Deploying ballot: ");
            var ballotDeployReceipt = web3.Eth.DeployContract.SendRequestAndWaitForReceiptAsync(BALLOT_ABI, BALLOT_BYTECODE, BALANCE_ADDRESS, new HexBigInteger(4700000), null, "Ballot").Result;
            var ballotAddress = ballotDeployReceipt.ContractAddress;
            LogLine(ConsoleColor.Magenta, ballotAddress);

            // Ballot interface
            Contract ballot = web3.Eth.GetContract(BALLOT_ABI, ballotAddress);
            Function ballotSetNameFunction = ballot.GetFunction("SetName");
            Function ballotNameFunction = ballot.GetFunction("Name");

            // Get message
            Log("Ballot.Name: ");
            string nallotName = ballotNameFunction.CallAsync<string>().Result;
            LogLine(ConsoleColor.DarkYellow, nallotName);
        }

        //--------------------------------------------------------------------------------
        private static void Log(params object[] arguments) {
            ConsoleColor previousForeColour = Console.ForegroundColor;
            ConsoleColor previousBackColour = Console.BackgroundColor;
            foreach (object a in arguments) {
                if (a is ConsoleColor)
                    Console.ForegroundColor = (ConsoleColor)a;
                else
                    Console.Out.Write(a);
            }
            Console.ForegroundColor = previousForeColour;
            Console.BackgroundColor = previousBackColour;
        }

        //--------------------------------------------------------------------------------
        private static void LogLine(params object[] arguments) {
            Log(arguments);
            Console.Out.WriteLine();
        }
        
        //--------------------------------------------------------------------------------
        private static Exception InnermostException(Exception e) {
            while (e.InnerException != null) {
                e = e.InnerException;
            }
            return e;
        }
    }
}
